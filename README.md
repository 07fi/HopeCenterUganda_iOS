

# Hope Center Uganda iOS App 

*This App is for Hope Center Uganda https://www.hopecenteruganda.org/*

#### Hope Center's Mission

Hope Center Uganda believes children don’t just need a hand out, but a hand up. We want to do this through providing the highest standard of care and Christian education. Donations are needed to not only help with general operating costs, but new projects that will grow the orphanage and provide more substantial opportunities to our children as they grow into adulthood.

#### Why Uganda, Why this APP 

Honestly this was not how I thought I was going to spend my summer. But sometimes life just grabs you and says **THIS IS IMPORTANT**. If it wasn't for the good men in my Wednesday night study group I would have not taken this thing on. I was really inspired by this [video](https://www.youtube.com/watch?v=h7UkTC_yrXE) that was shown one night. 

I was like **YES** this is what we should be doing! We should be helping people not by spending a lot of money by going to Africa but we should use our talents and gifts to help the people there and giving them the support and funds needed so they can live better. This is why I'm doing this.

#### For the Devlopers Viewing this Repo

This project is built with [Xamairin](https://www.xamarin.com/) using Microsoft Visual Studio (For Mac) Using C#. To get Facebook working you will need to get it from the [NuGet package listed](https://components.xamarin.com/view/facebookios) on the Xamarin Site. You will also need to follow the instructions in the [Getting Started - iOS](https://developers.facebook.com/docs/ios/getting-started) on the Facebook Dev site. For Google Firebase you will need to install the [Firebase Database for iOS](https://components.xamarin.com/view/firebaseiosdatabase) on the Xamarin Site. The documenation included on that page is enough to get you started. 

Currently as it stands if you throw this into Microsoft Visual Studio you will get **EXACTLY** what Im wokring on, expect this to change.

#### A word on the DEV Branch

The Dev branch includes most of my bleeding edge sort of working code. This stuff will compile and run but may not run as intended. I also include with every DEV push a Commit description that includes some of my thoughts on the Commit and will contain a lot of grammar and spelling errors. So if you can't deal with my RAW un-fettered thoughts then you may not want to look at them.



## Burn Down List

*Below you can see a Burn down list of all the features I hope to incorporate into the Hope Center iOS App. The items with the ~~strike~~ are completed.*

### App Wide

**v1.0**

* ~~Nav Bar~~
* Animations App Wide

**v1.1**

- ? Switch to React ?


### Home View

**v1.0**

* ~~Scrolling Content from facebook~~
* ~~Async Photo loading~~ 
- Auto Layout for Scrolling Content
- Dynamic loading/Unloading of multiple pages from the facebook api
- Ability to view Content in an optimal way in the scroll view dynamically.

**v1.1**

* Twitter Support
* Comment view support 
* Website Content 
* iPad Support 

### Donate View

**v1.0**

* Use Firebase to sync Data between website and Apps
* Have Static Content work in a way that is useable with our dynamic content
* Use Dynamic way to display donation options. 

### Contact View

**v1.0**

* Build out a view that allows users to contact the hope center through email

**v1.1**

* More ways to connect with hope center staff and kids

### About View

**v1.0**

* Copy pasta the stuff on the website to the App , Vid possible ???? .. (V1.1?)

