using Foundation;
using System;
using UIKit;
using CoreGraphics;

namespace HopeCenterUganda
{
    public partial class DonateSuper : UIView
    {
        public DonateSuper (IntPtr handle) : base (handle)
        {
			UIView Header = new UIView(new CGRect(0, 0, Frame.Width, 20));

			Header.BackgroundColor = UIColor.White;
            AddSubview(Header);
        }
    }
}