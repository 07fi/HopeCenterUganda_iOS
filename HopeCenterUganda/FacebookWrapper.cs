﻿using System;
using Foundation;
using Facebook.CoreKit;
namespace HopeCenterUganda
{
    public class FacebookWrapper
    {
        public GraphRequestConnection connection;
        public static NSDictionary Result { get; set; }
        public static NSError Error;

        public FacebookWrapper(string request)
        {
            connection = new GraphRequestConnection();
            if (connection != null)
            {
                Console.WriteLine("Initing Connection...");
                Console.WriteLine("Graph Request...  :  " + request);
                connection.AddRequest(new GraphRequest(request, null, "EAACxUXejpjsBAEbPEgQna8VNCqSjFj6lQ2GkF8ZCiyDUlt3TZA6q5eH5m4JbHrFfsAjDNWZAL9RhI9ogewDAaZC4ZB0O39aNoJ3wLpuZAmrJql3kX5Pl4aAIaKu7OBXyx0xq5TPelBoIfkdbdfaq7o3Xg5lCAef1QZD", "v2.9", "GET"), HandleGraphRequestHandler);
                connection.Start();


            }

        }
        public NSDictionary GetResult(){
            return Result;
        }
        public NSObject GetError(){
            return Error;
        }



        void HandleGraphRequestHandler(GraphRequestConnection connection, NSObject result, NSError error)
        {
            FacebookWrapper.Result = result as NSDictionary;
            FacebookWrapper.Error = error;

        }
    }

}
