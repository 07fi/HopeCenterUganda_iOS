// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace HopeCenterUganda
{
    [Register ("DonateViewController")]
    partial class DonateViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        HopeCenterUganda.Donate Donate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIScrollView DonateScroll { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        HopeCenterUganda.DonateSuper DonateSuper { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Donate != null) {
                Donate.Dispose ();
                Donate = null;
            }

            if (DonateScroll != null) {
                DonateScroll.Dispose ();
                DonateScroll = null;
            }

            if (DonateSuper != null) {
                DonateSuper.Dispose ();
                DonateSuper = null;
            }
        }
    }
}