// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace HopeCenterUganda
{
    [Register ("FirstController")]
    partial class FirstController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        HopeCenterUganda.HomeS HomeScroll { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        HopeCenterUganda.potatoMain potato { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (HomeScroll != null) {
                HomeScroll.Dispose ();
                HomeScroll = null;
            }

            if (potato != null) {
                potato.Dispose ();
                potato = null;
            }
        }
    }
}