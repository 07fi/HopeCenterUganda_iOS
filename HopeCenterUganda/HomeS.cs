using Foundation;
using System;
using System.Collections.Generic;
using UIKit;
using CoreGraphics;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Threading;

namespace HopeCenterUganda
{
    public partial class HomeS : UIScrollView
    {
        

        public HomeS (IntPtr handle) : base (handle)
        {



            FacebookWrapper facebookthing = new FacebookWrapper("Hopecenteruganda/feed?fields=created_time,message,id,full_picture");
            UIStackView HomeStack = new UIStackView();
            HomeStack.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
            HomeStack.AutosizesSubviews = true;
            HomeStack.Axis = UILayoutConstraintAxis.Vertical;
            HomeStack.Alignment = UIStackViewAlignment.Center;
            HomeStack.Distribution = UIStackViewDistribution.FillProportionally;
            HomeStack.Spacing = 2;
			AddSubview(HomeStack);
			//HomeStack.ConstrainLayout(() => HomeStack.Frame.Width == this.Frame.Width && HomeStack.Frame.Height == this.Frame.Height);
			Console.WriteLine(HomeStack.Frame);


			facebookthing.connection.Failed += (sender, e) => {
                
                UILabel ConnectionFailed = new UILabel(new CGRect(10, 110, Frame.Width - 20, 100));
                ContentSize = new CGSize(HomeStack.Frame.Width,  300);
                ConnectionFailed.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
                ConnectionFailed.TextAlignment = UITextAlignment.Center;
                ConnectionFailed.Text = "Connection Failed";
                ConnectionFailed.TextColor = UIColor.Gray;
				HomeStack.AddSubview(ConnectionFailed);



            };

			facebookthing.connection.LoadingFinished += (NSURLAuthenticationChallengeSender, er) =>
            {
                NSDictionary thing = facebookthing.GetResult();

                NSArray thing0 = thing.Values.GetValue(0) as NSArray;
                NSDictionary thing1 = thing.Values.GetValue(1) as NSDictionary;

                int count = 0;
                int blue = 150;
                bool up = true;

				for (int x = 0; x < (int)thing0.Count; x++ ) {
                    
                    var thinggy = thing0.GetItem<NSDictionary>((System.nuint)x);
                    Console.WriteLine(thinggy);
                    Post newPost = new Post(new CGRect(10, count, Frame.Width - 20, 250));
					// Lots to do here .. Something with aysnc

         
                    try{
                        newPost.Message = thinggy.ValueForKey((NSString)"message").ToString();
                        newPost.ID = thinggy.ValueForKey((NSString)"id").ToString();
                        newPost.Full_Picture = thinggy.ValueForKey((NSString)"full_picture").ToString();
                        newPost.Created_time = DateTime.Parse(thinggy.ValueForKey((NSString)"created_time").ToString());
                   
                    }catch{
                        
                    };



					if (up == true)
					{
						blue += 10;
					}
					if (up == false)
					{
						blue -= 10;
					}
                    if (blue <= 150)
                    {
                        up = true;
                        blue = 150;
                    }
                    if (blue >= 255)
                    {
                        up = false;
                        blue = 255;
                    }



                    //Create Sub views that pretain to the new ui view
                    newPost.SetBackgroundColor(UIColor.FromRGB(45, 156, blue));
                    newPost.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
                    newPost.AutosizesSubviews = true;
                    newPost.lab.Text = newPost.Message;
                    newPost.LoadImage();
                    HomeStack.AddArrangedSubview(newPost);

                    HomeStack.BackgroundColor = UIColor.Clear;


                    count += (int)newPost.Hight + 20;
                    ContentSize = new CGSize(HomeStack.Frame.Width, count+300);
                    HomeStack.Frame = new CGRect(new CGPoint(10,100),new CGSize(HomeStack.Frame.Width, count));


				}
            };

        }

    }
}