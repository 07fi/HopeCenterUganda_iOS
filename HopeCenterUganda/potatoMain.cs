using Foundation;
using System;
using UIKit;
using CoreGraphics;
namespace HopeCenterUganda
{
    public partial class potatoMain : UIView
    {
        public potatoMain(IntPtr handle) : base(handle)
        {
            
            UIView Header = new UIView(new CGRect(0, 0, Frame.Width, 95));

            Header.BackgroundColor = UIColor.White;

			UIImageView HopeCenterLogo = new UIImageView(UIImage.FromBundle("Logo"));
            Header.AddSubview(HopeCenterLogo);
            AddSubview(Header);
            Header.ConstrainLayout(() => HopeCenterLogo.Frame.GetCenterX() == Header.Frame.GetCenterX() && 
                                   HopeCenterLogo.Frame.Bottom >= Header.Frame.Bottom - 5 &&
                                   HopeCenterLogo.Frame.Top == Header.Frame.Top + 20);
			Header.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;

		}

    }
}