﻿using System;
using UIKit;
using CoreGraphics;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http;
using Foundation;
using System.Drawing;
namespace HopeCenterUganda
{
    public class Post : UIView
    {

        public DateTime Created_time { get; set; }
        public String Message { get; set; }
        public String ID { get; set; }
        public String Full_Picture { get; set; }
        public UIImage Pic { get; set; }
        public UIImageView PicView { get; set; }
        public UILabel lab { get; set; }
        public UIView Background { get; set; }
        public CancellationTokenSource Cts { get; set; }
        public float Hight { get; set; }
        public float Width { get; set; }
        public override CGSize IntrinsicContentSize => new CGSize(Width, Hight);

        //Thie is a object class to describe facebook posts
        public Post(CGRect size)
        {
            this.AutosizesSubviews = true;

            this.Frame = size;
            this.Hight = (float)Frame.Height;
            this.Width = (float)Frame.Width;
            //Console.WriteLine(IntrinsicContentSize);
            this.Background = new UIView(new CGRect(0, 0, Frame.Width,Frame.Height));
            this.AddSubview(Background);
            this.lab = new UILabel(new CGRect(0, 150, Frame.Width, Frame.Height-150));
            this.lab.TextAlignment = UITextAlignment.Natural;
            this.lab.LineBreakMode = UILineBreakMode.WordWrap;
            this.lab.Lines = 3;
            this.lab.TextColor = UIColor.White;

            this.AddSubview(lab);


            this.PicView = new UIImageView(new CGRect(0, 0, Frame.Width, Frame.Height));
            this.PicView.BackgroundColor = UIColor.Clear;
            this.PicView.ContentMode = UIViewContentMode.ScaleAspectFill;
            this.PicView.ClipsToBounds = true;
            this.AddSubview(PicView);
            this.SendSubviewToBack(Background);
            this.BringSubviewToFront(PicView);
            this.BringSubviewToFront(lab);

		}
        public void SetBackgroundColor(UIColor color){
        
            Background.BackgroundColor = color;
            lab.BackgroundColor = color.ColorWithAlpha(0.5f);
        
        }
        public void SetPicture(UIImage pic){
            
            this.PicView.Image = pic;

        }
        public void LoadImage(){
			if (Full_Picture != null)
			{
                Task.Run(async () => 
                {
                    try{
						UIImage img = await Images.LoadImage(Full_Picture);
                        InvokeOnMainThread(() =>
                        {
                            SetPicture(img);
                            //Console.WriteLine("We set the pic !!!");
                        });
                                           
                                 
						
                    }catch (System.OperationCanceledException ex)
					{
						Console.WriteLine($"Image load cancelled: {ex.Message}");
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex.Message);
					}


                });
			} 
        }

   }
}
